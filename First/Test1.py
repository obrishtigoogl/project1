import subprocess
import datetime


def is_number(str):
    try:
        float(str)
        return True
    except ValueError:
        return False


nmap = subprocess.run(["nmap", "-sP", "192.168.0.0/24"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                      encoding="utf-8")
arp = subprocess.run(["arp", "-na"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8")
output0 = nmap.stdout
output1 = arp.stdout

a = list(output1.split("\n"))
itoglist = [["ip"], ["MAC"], ["last_onlisne"]]
now = datetime.datetime.now()

# сортировка добавление даты
for b in a:
    c = list(b.split())
    if len(c) != 0:
        itoglist[0].append(c[1])
        itoglist[1].append(c[3])
        if len(' '.join(c[8:10])) != 0:
            time = datetime.datetime.strptime(' '.join(c[8:10]), "%Y-%m-%d %H:%M")
        else:
            time = '-'
        itoglist[2].append(time)
sort_online_time = itoglist.copy()
sort_online_time.append(["in_hours"])

# Количество часов
for t in sort_online_time[2][1:]:
    if isinstance(t, datetime.datetime) == True:
        dat = now - t
        # print(t)
        seconds = dat.total_seconds()
        dat = seconds // 3600
    else:
        dat = "-"
        # print(t)
    sort_online_time[3].append(dat)
# datetime.datetime неправильно отбражается при таком выводе
sort_online_time[2] = [str(g) for g in sort_online_time[2]]
sort_online_time[3] = [str(g) for g in sort_online_time[3]]

itog = zip(sort_online_time[0], sort_online_time[1], sort_online_time[2], sort_online_time[3])
# for i in itog:
#  print("{:<20}{:<20}{:<30}{:<30}".format(*i))
print("Компютеры в сети за последние сутки")

for i in itog:
    if is_number(i[3]) == True and is_number(i[3]) < 24 or i[3] == 'in_hours':
        print("{:<20}{:<20}{:<30}{:<30}".format(*i))
